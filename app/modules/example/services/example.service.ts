import type { IHouse } from '../model/house.model';
import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react';

export const houseAPi = createApi({
  reducerPath: 'houseAPi',
  baseQuery: fetchBaseQuery({ baseUrl: 'http://localhost:8080/' }),
  tagTypes: ['IHouse'],
  endpoints: (builder) => ({
    getHouses: builder.query<IHouse[], void>({
      query: () => "house",
      providesTags: ['IHouse'],
    }),
    addHouse: builder.mutation<IHouse, Omit<IHouse, 'id'>>({
      query(body) {
        return {
          url: `house/add`,
          method: "POST",
          body
        };
      },
      invalidatesTags:['IHouse']
    }),
    deletHouse: builder.mutation<{ id: number }, void>({
      query(id) {
        return {
          url: `house/delete/${id}`,
          method: "DELETE",
        };
      },
      invalidatesTags:['IHouse']
    }),
  }),
});

export const { useGetHousesQuery,  useAddHouseMutation, useDeletHouseMutation } = houseAPi;
