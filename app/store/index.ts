import { configureStore } from '@reduxjs/toolkit';
import { houseAPi } from '../modules/example/services/example.service';
import stayAppSlice from '../modules/example/store';
import logger from './middleware/logger';

export const store = configureStore({
  reducer: {
    stayApp: stayAppSlice,
    [houseAPi.reducerPath]: houseAPi.reducer,
  }
  ,
  middleware: getDefaultMiddleware =>
    getDefaultMiddleware()
      // prepend and concat calls can be chained
      .concat(logger, houseAPi.middleware)
})
// Infer the `RootState` and `AppDispatch` types from the store itself
export type RootState = ReturnType<typeof store.getState>
// Inferred type: {posts: PostsState, comments: CommentsState, users: UsersState}
export type AppDispatch = typeof store.dispatch