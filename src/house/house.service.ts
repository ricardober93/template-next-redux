import { Injectable } from '@nestjs/common';

export interface IHouse {
  id: number;
  title: string;
  city: string;
  mount: number;
  select: boolean;
}
@Injectable()
export class HouseService {

  house: IHouse[] = [];

  findAll() {
    return this.house;
  }

  create(data: IHouse) {
    this.house.push(data);
  }

  delete(id: number) {
    this.house =  this.house.filter((h) => h.id !== Number(id));
  }
}
