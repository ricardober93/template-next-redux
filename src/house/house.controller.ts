import {
  Body,
  Controller,
  Delete,
  Get,
  HttpCode,
  Param,
  Post
} from '@nestjs/common';
import { HouseService } from './house.service';

@Controller('house')
export class HouseController {
  constructor(private readonly houseService: HouseService) {}

  @Get('')
  @HttpCode(200)
  FindAll() {
    return this.houseService.findAll();
  }
  @Post('add')
  Create(@Body() body) {
    return this.houseService.create(body);
  }
  @Delete('delete/:id')
  Delele(@Param() param) {
    this.houseService.delete(param.id);
  }
}
